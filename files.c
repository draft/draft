/***************************************************************************
 *            files.c
 *
 *  Wed September 25 11:09:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/

#include <stdlib.h>
#include "files.h"

/* 
 * Simply calls add_editor() for a new file
 */
DraftEditor* new_file(GtkAction *action, DraftApp *app)
{    
    DraftEditor *editor = add_editor(app, "New Draft", FALSE, NULL);
    gtk_widget_show_all (app->notebook);
    return editor;
}

gboolean find_editor_by_filename(gpointer page, DraftEditor *editor, gchar *filename)
{
    int equals = g_strcmp0(filename, editor->filename);
    if (equals==0)
        return TRUE;
    else
        return FALSE;
}

/*
 * Open a file choose dialog for selecting a file to load, then calls
 * load_file() for loading it into an editor.
 */
void open_file_dialog(GtkAction *action, DraftApp *app)
{
    GtkWidget *dialog;
    gchar *filename;

    dialog = gtk_file_chooser_dialog_new ("Open File",
                                        GTK_WINDOW(app->window),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                        GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
                                        NULL);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        
        // Looks up editor list to find out if selected file is already opened
        DraftEditor *editor = g_hash_table_find(app->editors, (GHRFunc)find_editor_by_filename, filename);
        if (editor)
            gtk_notebook_set_current_page(GTK_NOTEBOOK(app->notebook), editor->page);
        else
            load_file(app, filename);
    }
    gtk_widget_destroy (dialog);

}

/*
 * Loads the file passed as input, guess language for syntax highlight and
 * calls add_editor() for creating an editor/tab for the file.
 */
DraftEditor* load_file(DraftApp *app, gchar *filename)
{
    gchar *text;
    GError *error = NULL;
    DraftEditor *new_editor = NULL;

    if (filename!=NULL) {
        
        if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
            GError *error = g_error_new(g_quark_from_string("draft"), 
                                            DRAFT_ERROR_EXISTS, 
                                            "Failed to open %s",
                                            filename);
            report_error(app, error);
        }
        
        if (!g_path_is_absolute(filename)) {
            char actualpath [PATH_MAX+1];
            char *ptr;
            
            ptr = realpath(filename, actualpath);
        }
        
        filename = g_convert (filename, -1, "UTF-8", "ISO-8859-1", NULL, NULL, &error);
    
        FILE *file = g_fopen (filename, "r");

        if (file) {
            g_file_get_contents(filename, &text, NULL , &error);
            fclose(file);

            GtkSourceLanguage *language = gtk_source_language_manager_guess_language(app->language_manager, filename, NULL);

            new_editor = add_editor (app, filename, TRUE, language);

            gtk_text_buffer_set_text(GTK_TEXT_BUFFER(new_editor->buffer), text, -1); 
            g_signal_connect(new_editor->buffer, "changed", G_CALLBACK(on_text_changed), app);

            g_free (text); 

            gtk_action_set_sensitive(app->action_save,FALSE);
            clear_message(app, "SAVE");
            update_title(app);
        }
    }
    if (new_editor == NULL)
        new_editor = new_file(NULL, app);
    return new_editor;
}

/*
 * Writes the content of editor to filesystem
 */
void write_file(DraftApp *app, DraftEditor *editor)
{
    GtkTextIter start;
    GtkTextIter end;
    gchar *text;

    if (editor->filename) {
        FILE *file; 
        file = g_fopen(editor->filename,"w+");

        if (file){
            gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(editor->buffer), &start);
            gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(editor->buffer), &end);
            text = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(editor->buffer), &start, &end, TRUE);

            fputs (text, file);

            fclose (file);

            gtk_action_set_sensitive(app->action_save,FALSE);
            gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(editor->buffer), FALSE);

            editor->exists = TRUE;

            show_message(app, "File saved.", "SAVE");
            update_title(app);
            update_tab_label(app, editor);

            g_free(text);
        }
    }
}

/* 
 * Opens a dialog for choosing where to save the content of the editor.
 */
void save_file_dialog(DraftApp *app, DraftEditor *editor)
{
    GtkWidget *dialog;
    gchar *filename;
    GError *error = NULL;
    
    dialog = gtk_file_chooser_dialog_new ("Save File",
                      GTK_WINDOW(app->window),
                      GTK_FILE_CHOOSER_ACTION_SAVE,
                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                      NULL);

    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
    {
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        filename = g_convert (filename, -1, "UTF-8", "ISO-8859-1", NULL, NULL, &error);
        
        editor->filename = filename;
        write_file(app, editor);
        
    }

    gtk_widget_destroy (dialog);
}

/*
 * if editor is assiciated to a real file (gboolean exists property), it calls
 * directly write_file. Otherwise open a dialog for choosing filename.
 */
void save_file(GtkAction *action, DraftApp *app)
{
    if (app->current_editor->exists) {
        write_file(app, app->current_editor); //TODO salva
    } else {
        save_file_dialog(app, app->current_editor);
    }
}