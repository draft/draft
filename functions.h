/***************************************************************************
 *            functions.h
 *
 *  Wed September 14 16:13:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/
#ifndef FUNCTIONS_H 
#define FUNCTIONS_H 

#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguagemanager.h>
#include <gconf/gconf-client.h>

GConfClient *gconf;
#define GCONF_NAME(x) "/apps/draft/" #x
#define DRAFT_ERROR_EXISTS 1

typedef struct {
    gboolean line_num;
    gboolean wrap_word;
    gboolean right_margin;
} DraftConfig;

typedef struct {
    GtkSourceBuffer *buffer;
    GtkWidget *textview;
    gint      page;
    gboolean  exists;
    gchar     *filename;
} DraftEditor;
 
typedef struct {
    GtkWidget *window;
    GtkWidget *vbox;
    GtkWidget *statusbar;
    GtkWidget *toolbar;
    GtkWidget *tog_wrap;
    GtkWidget *tog_num;
    GtkWidget *tog_margin;
    GtkWidget *notebook;
    GtkAction *action_new;
    GtkAction *action_open;
    GtkAction *action_save;
    GtkMenu   *menu_view;
    gchar     *current_file;
    GtkSourceLanguageManager *language_manager;
    GHashTable *editors;
    DraftEditor *current_editor;
    DraftConfig *config;
} DraftApp;

DraftEditor* add_editor( DraftApp *, gchar * , gboolean, GtkSourceLanguage *);

void report_error(DraftApp *, GError* );

void show_message( DraftApp *, gchar *, gchar *);

void clear_message( DraftApp* , gchar *);

void on_text_changed( GtkTextBuffer *, DraftApp *);

void quit_program( GtkWindow *, DraftApp *);

void close_window( GtkWindow *, GdkEvent *, DraftApp *);

void update_title( DraftApp *);

void switch_page( GtkNotebook *, gpointer , guint , DraftApp * );

void on_cut( GtkAction *, DraftApp *);

void on_copy( GtkAction *, DraftApp *);

void on_paste( GtkAction *, DraftApp *);

void on_delete( GtkAction *, DraftApp *);

void on_undo( GtkAction *, DraftApp* );

void on_redo( GtkAction *, DraftApp *);

void on_has_selection_notify( GtkTextBuffer *, GParamSpec *, DraftApp *);

void update_tab_label( DraftApp *, DraftEditor * );

gint show_menu_view( DraftApp *, GdkEvent *);

void show_about_dialog( GtkMenuItem *, gpointer );

GdkPixbuf *create_pixbuf( const gchar *);

#endif