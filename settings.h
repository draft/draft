/***************************************************************************
 *            settings.h
 *
 *  Wed September 25 11:09:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "functions.h"

void reload_config( DraftApp *app );

void apply_config_to_editor( gpointer , DraftEditor *, DraftConfig * );

void update_preferences_toggles( DraftApp *, DraftConfig * );

void set_editor_preferences( DraftApp *, DraftEditor *);

void load_preferences( DraftApp *);

void toggle_wrap( GtkMenuItem *, DraftApp * );

void toggle_line_num(GtkMenuItem *, DraftApp * );

void toggle_margin( GtkMenuItem *, DraftApp * );

#endif