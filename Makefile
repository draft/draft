CC = gcc
CFLAGS = -g -Wall
OBJECTS = main.o functions.o files.o settings.o

draft: $(OBJECTS)
	$(CC) -o draft $(OBJECTS) \
	`pkg-config --libs gtk+-3.0 gtksourceview-3.0 gconf-2.0`

main.o: main.c functions.h settings.h files.h
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0 gtksourceview-3.0 gconf-2.0` -c main.c

functions.o: functions.c functions.h files.h settings.h
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0 gtksourceview-3.0 gconf-2.0` -c functions.c

settings.o: settings.c settings.h
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0 gtksourceview-3.0 gconf-2.0` -c settings.c

files.o: files.c files.h
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0 gtksourceview-3.0 gconf-2.0` -c files.c
