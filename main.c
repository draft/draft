/*
 * main.c
 * Copyright (C) Riccardo Albertini 2011 <ssirowain@gmail.com>
 * 
 * Draft is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Draft is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h> 
#include <glib/gi18n.h>
#include "functions.h"
#include "settings.h"
#include "files.h"

#define UI_FILE "main.glade"
#define APP_NAME "Draft";

static GtkWidget*
create_window (int argc, char *argv[])
{
    GtkBuilder  *builder;
    GError      *error = NULL;
    DraftApp    *app;

    GtkWidget   *button_new;
    GtkWidget   *button_open;
    GtkWidget   *button_save;

    app = g_slice_new (DraftApp);
    app->config = g_slice_new (DraftConfig);
    app->current_file = NULL;

    // create hash table for editor list.
    app->editors = g_hash_table_new(NULL, NULL);

    builder = gtk_builder_new();
    if( ! gtk_builder_add_from_file( builder, UI_FILE, &error ) )
    {
        g_warning( "%s", error->message );
        g_free( error );
        return( NULL );
    }
    app->window = GTK_WIDGET( gtk_builder_get_object( builder, "window" ) );
 
    gtk_builder_connect_signals( builder, NULL );
 
    gtk_widget_show( app->window );
    
    app->statusbar = GTK_WIDGET( gtk_builder_get_object( builder, "statusbar" ) );
    app->toolbar = GTK_WIDGET( gtk_builder_get_object( builder, "toolbar" ) );
    app->vbox = GTK_WIDGET( gtk_builder_get_object( builder, "vbox" ) );
    app->language_manager = gtk_source_language_manager_new();

    app->action_new = GTK_ACTION( gtk_builder_get_object( builder, "action_new" ) );
    app->action_open = GTK_ACTION( gtk_builder_get_object( builder, "action_open" ) );
    app->action_save = GTK_ACTION( gtk_builder_get_object( builder, "action_save" ) );

    button_new = GTK_WIDGET( gtk_builder_get_object( builder, "button_new" ) );
    button_open = GTK_WIDGET( gtk_builder_get_object( builder, "button_open" ) );
    button_save = GTK_WIDGET( gtk_builder_get_object( builder, "button_save" ) );
    app->menu_view = GTK_MENU( gtk_builder_get_object( builder, "menu_view" ) );
    
    app->tog_wrap = GTK_WIDGET( gtk_builder_get_object( builder, "menuitem_wrap" ) );
    app->tog_num = GTK_WIDGET( gtk_builder_get_object( builder, "menuitem_lines" ) );
    app->tog_margin = GTK_WIDGET( gtk_builder_get_object( builder, "menuitem_margin" ) );
    
    /* TEXT VIEW */
    app->notebook = GTK_WIDGET( gtk_builder_get_object( builder, "notebook" ) );
    
    DraftEditor *editor;
    if (argc>1)
        editor = load_file(app, argv[1]);
    else
        editor = add_editor (app, "New Draft", FALSE, NULL);
    
    // GtkSourceBuffer *buf = (GtkSourceBuffer*)gtk_text_view_get_buffer(GTK_TEXT_VIEW(editor->textview));
    
    //gtk_action_set_sensitive(app->action_paste, gtk_clipboard_wait_is_text_available(gtk_clipboard_get(GDK_SELECTION_CLIPBOARD)));

    gtk_widget_grab_focus(GTK_WIDGET(editor->textview));
    
    /* SIGNALS */
    
    //g_signal_connect(app->window, "delete-event", G_CALLBACK(close_window), app);

    g_signal_connect(app->window, "destroy", G_CALLBACK(gtk_main_quit), app);
    
    g_signal_connect(app->action_new, "activate", G_CALLBACK(new_file), app);
    g_signal_connect(button_new, "clicked", G_CALLBACK(new_file), app);
    g_signal_connect(app->action_open, "activate", G_CALLBACK(open_file_dialog), app);
    g_signal_connect(button_open, "clicked", G_CALLBACK(open_file_dialog),app);
    g_signal_connect(app->action_save, "activate", G_CALLBACK(save_file), app);
    g_signal_connect(button_save, "clicked", G_CALLBACK(save_file), app);

    g_signal_connect(app->tog_wrap, "activate", G_CALLBACK(toggle_wrap), app);
    g_signal_connect(app->tog_num, "activate", G_CALLBACK(toggle_line_num), app);
    g_signal_connect(app->tog_margin, "activate", G_CALLBACK(toggle_margin), app);

    g_signal_connect(app->notebook, "switch-page", G_CALLBACK(switch_page), app);
    
    g_object_unref(G_OBJECT(builder));
    return app->window;
}

int
main (int argc, char *argv[])
{
     GtkWidget *window;

#ifdef ENABLE_NLS
    bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);
#endif
    
    gtk_init (&argc, &argv);

    window = create_window (argc, argv);
    
    gtk_widget_show_all (window);

    gtk_main ();
    return 0;
}
