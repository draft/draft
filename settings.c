/***************************************************************************
 *            settings.c
 *
 *  Wed September 25 11:09:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/

#include "settings.h"

void apply_config_to_editor(gpointer page, DraftEditor *editor, DraftConfig *config)
{
	// wrap mode
	if (config->wrap_word)
		gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(editor->textview), GTK_WRAP_WORD_CHAR);
	else
		gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(editor->textview), GTK_WRAP_NONE);

	// line numbers
	gtk_source_view_set_show_line_numbers(GTK_SOURCE_VIEW(editor->textview), config->line_num);

	// right margin
	gtk_source_view_set_show_right_margin(GTK_SOURCE_VIEW(editor->textview), config->right_margin);
}

void reload_config(DraftApp *app)
{
	load_preferences(app);
	g_hash_table_foreach(app->editors, (GHFunc)apply_config_to_editor, app->config);
}

void set_editor_preferences(DraftApp *app, DraftEditor *editor)
{
	/* GCONF SETTINGS LOAD */
	gconf = gconf_client_get_default();
	reload_config(app);

	gtk_source_view_set_insert_spaces_instead_of_tabs(GTK_SOURCE_VIEW(editor->textview), TRUE);
	gtk_source_view_set_indent_width(GTK_SOURCE_VIEW(editor->textview), 4);
	gtk_source_view_set_auto_indent(GTK_SOURCE_VIEW(editor->textview), TRUE);

	PangoFontDescription *font_desc = pango_font_description_from_string ("monospace 9");
	gtk_widget_modify_font (editor->textview, font_desc);     
	pango_font_description_free (font_desc);
}

void update_preferences_toggles(DraftApp *app, DraftConfig *config)
{
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(app->tog_num), config->line_num);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(app->tog_wrap), config->wrap_word);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(app->tog_margin), config->right_margin);
}

void load_preferences(DraftApp *app)
{
	app->config->line_num = gconf_client_get_bool(gconf, GCONF_NAME(line_num), NULL);
	app->config->wrap_word = gconf_client_get_bool(gconf, GCONF_NAME(wrap_word), NULL);
	app->config->right_margin = gconf_client_get_bool(gconf, GCONF_NAME(right_margin), NULL);
}

void toggle_wrap(GtkMenuItem *widget, DraftApp *app)
{
	gboolean state = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));
	
	gconf_client_set_bool(gconf, GCONF_NAME(wrap_word), state, NULL);

	reload_config(app);
}

void toggle_line_num(GtkMenuItem *widget, DraftApp *app)
{
	gboolean state = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));

	gconf_client_set_bool(gconf, GCONF_NAME(line_num), state, NULL);

	reload_config(app);
}

void toggle_margin(GtkMenuItem *widget, DraftApp *app)
{
	gboolean state = gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(widget));

	gconf_client_set_bool(gconf, GCONF_NAME(right_margin), state, NULL);

	reload_config(app);
}
