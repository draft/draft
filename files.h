/***************************************************************************
 *            files.h
 *
 *  Wed September 25 11:09:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/
#ifndef FILES_H
#define FILES_H

#include "settings.h"

DraftEditor* new_file( GtkAction *, DraftApp *);

void open_file_dialog( GtkAction *, DraftApp *);

DraftEditor* load_file( DraftApp *, gchar *);

void write_file( DraftApp *, DraftEditor * );

void save_file_dialog( DraftApp *, DraftEditor *);

void save_file( GtkAction *, DraftApp *);

#endif