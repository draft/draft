/***************************************************************************
 *            functions.c
 *
 *  Wed September 14 16:13:43 2011
 *  Copyright Riccardo Albertini 2011  
 *  <ssirowain@gmail.com>
 ****************************************************************************/

#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <string.h>
#include <stdlib.h>
#include "functions.h"
#include "settings.h"
#include "files.h"

GtkWidget *error_info_bar;
GtkWidget *error_label;

DraftEditor* add_editor(DraftApp *app, gchar *filename, gboolean exists, GtkSourceLanguage *language)
{
    GtkWidget *scrolled;
    
    DraftEditor *new_editor;
    new_editor = g_slice_new (DraftEditor);
    new_editor->filename = filename;
    if (language)
        new_editor->buffer = gtk_source_buffer_new_with_language(GTK_SOURCE_LANGUAGE(language));
    else 
        new_editor->buffer = gtk_source_buffer_new(NULL);
    new_editor->textview = gtk_source_view_new_with_buffer(new_editor->buffer);
    scrolled = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(scrolled), new_editor->textview);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    new_editor->page = gtk_notebook_append_page(GTK_NOTEBOOK(app->notebook), scrolled, gtk_label_new(g_path_get_basename(filename)));
    g_hash_table_insert (app->editors, GINT_TO_POINTER(new_editor->page), new_editor);

    new_editor->exists = exists;
    app->current_editor = new_editor;
    gtk_widget_show_all (app->notebook);

    gtk_notebook_set_current_page(GTK_NOTEBOOK(app->notebook), new_editor->page);

    // Preferences
    set_editor_preferences(app, new_editor);
    update_preferences_toggles(app, app->config);
    
    return new_editor;
}

static void on_info_bar_response(GtkWidget *error_info_bar, gint response, gpointer data)
{
    if (response == GTK_RESPONSE_OK)
    {
        gtk_widget_destroy(error_info_bar);
        error_info_bar = NULL;
    }
}

void report_error(DraftApp *app, GError* error)
{
    if (error == NULL)
    {
        return;
    }

    else
    {
        error_info_bar = gtk_info_bar_new_with_buttons(GTK_STOCK_OK,
                                                       GTK_RESPONSE_OK,
                                                       NULL);
        g_signal_connect(error_info_bar, "response", G_CALLBACK(on_info_bar_response), NULL);
        gtk_info_bar_set_message_type(GTK_INFO_BAR(error_info_bar),
                                      GTK_MESSAGE_ERROR);

        error_label = gtk_label_new(error->message);
        GtkWidget *container = gtk_info_bar_get_content_area(GTK_INFO_BAR(error_info_bar));
        gtk_container_add(GTK_CONTAINER(container), error_label);

        gtk_box_pack_start(GTK_BOX(app->vbox), error_info_bar, FALSE, FALSE, 0);
        gtk_box_reorder_child(GTK_BOX(app->vbox), error_info_bar, 2);
        gtk_widget_show_all(app->vbox);
    }
}

void show_message(DraftApp *app, gchar *text, gchar *descr)
{
    int uid = gtk_statusbar_get_context_id(GTK_STATUSBAR(app->statusbar), descr);

    gtk_statusbar_push(GTK_STATUSBAR(app->statusbar), uid, text);
}

void clear_message(DraftApp *app, gchar *descr)
{
    int uid = gtk_statusbar_get_context_id(GTK_STATUSBAR(app->statusbar), descr);
    gtk_statusbar_pop(GTK_STATUSBAR(app->statusbar), uid);
}

void on_text_changed(GtkTextBuffer *widget, DraftApp *app)
{
    clear_message(app, "SAVE");
}

void quit_program(GtkWindow *widget, DraftApp *app)
{
    /*GtkWidget *dialog, *content_area, *label_1, *label_2;

    GtkSourceBuffer *buf = (GtkSourceBuffer*)gtk_text_view_get_buffer(GTK_TEXT_VIEW(app->textview));

    if (gtk_text_buffer_get_modified (GTK_TEXT_BUFFER(buf))) {
        dialog = gtk_dialog_new_with_buttons ("Quit",
                          GTK_WINDOW(app->window),
                          GTK_DIALOG_DESTROY_WITH_PARENT,
                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                          GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
                          NULL);

        content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
        label_1 = gtk_label_new ("The document has some unsaved changes.");
        label_2 = gtk_label_new ("Exit anyway?");
        gtk_container_add (GTK_CONTAINER (content_area), label_1);
        gtk_container_add (GTK_CONTAINER (content_area), label_2);
        gtk_widget_show_all (dialog);

        if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
            gtk_widget_destroy (dialog);
            gtk_main_quit();
        } else {
            gtk_widget_destroy (dialog);
        }

    } else {
        gtk_main_quit();
    }*/
    gtk_main_quit();

}

void close_window(GtkWindow *widget, GdkEvent *event, DraftApp *app)
{
    quit_program(widget, app);
}

void switch_page( GtkNotebook *notebook, gpointer page, guint pagenum, DraftApp *app)
{
    DraftEditor *editor = g_hash_table_lookup(app->editors, GINT_TO_POINTER(pagenum));
    app->current_editor = editor;
    update_title (app);
}

void update_title(DraftApp *app)
{
    gchar *current_file = app->current_editor->filename;
    gchar *app_name = " - Draft";
    gchar **d = g_strsplit(current_file, g_get_home_dir(), -1);
    current_file = g_strjoinv("~", d);
    gchar *title = malloc(strlen(app_name) + strlen(current_file) + 2);
    strcpy(title, current_file);
    strcat(title, app_name);
    g_strfreev(d);
    gtk_window_set_title(GTK_WINDOW(app->window), title);
}

void on_cut(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "cut-clipboard", NULL);
}

void on_copy(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "copy-clipboard", NULL);
}

void on_paste(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "paste-clipboard", NULL);
}

void on_delete(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "delete-from-cursor", NULL);
}

void on_undo(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "undo", NULL);
}

void on_redo(GtkAction *action, DraftApp *app)
{
    g_signal_emit_by_name(app->current_editor->textview, "redo", NULL);
}

void on_has_selection_notify(GtkTextBuffer *buffer, GParamSpec *pspec, DraftApp *app)
{
    //gboolean has_selection = gtk_text_buffer_get_has_selection(buffer);
    
}

void update_tab_label(DraftApp *app, DraftEditor *editor)
{
    GtkWidget *page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(app->notebook), editor->page);
    gchar *filename = g_path_get_basename(editor->filename);
    gtk_notebook_set_tab_label_text(GTK_NOTEBOOK(app->notebook), page, filename); 
}

void show_about_dialog(GtkMenuItem *widget, gpointer dialog)
{
    gint result = gtk_dialog_run (GTK_DIALOG (dialog));
    switch (result)
    {
        case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_hide (dialog);
            break;
        case GTK_RESPONSE_CANCEL:
            gtk_widget_hide (dialog);
            break;
        default:
            break;
    }     
}

GdkPixbuf *create_pixbuf(const gchar * filename)
{
    GdkPixbuf *pixbuf;
    GError *error = NULL;
    pixbuf = gdk_pixbuf_new_from_file(filename, &error);

    if (!pixbuf) {
        fprintf(stderr, "%s\n", error->message);
        g_error_free(error);
    }

    return pixbuf;
}
